#!/usr/bin/with-contenv bash

echo "username: $MONGO_USERNAME"
echo "password: $MONGO_PASSWORD"

cd /localdb-tools/setting || exit
MONGO=mongosh 
export MONGO


export username='test'
export password='test'
#printf "y\ntest\ntest\n" | ./create_admin.sh
printf "y\n%s\n%s\n" "$username" "$password" | ./create_admin.sh


cd /yarr/localdb || exit
#printf "admin\npass\n" | 
bash ./login_mongodb.sh

cd /localdb-tools/viewer || exit
printf "y\n%s\n%s\n" "$username" "$password" | ./setup_viewer.sh

python3 /localdb-tools/viewer/app.py --config /localdb-tools/viewer/user_conf.yml

