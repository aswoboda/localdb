import os

DEFAULT_PATH = r"/Users/annaswoboda/cernbox/PhD/07_scripts/localdb_git/localdb/mongodb/input/009769_std_digitalscan"
DEFAULT_FES = ["0x1F4", "0x1F5", "0x1F6", "0x1F7"]

def getPath():
    inp = input("Please enter the path to your measurement and press Enter. If no input is detected, default path will be used.")
    if inp == "":
        path = DEFAULT_PATH
        print(path)
    else:
        path = inp
        print(path)
    return path


def loop(path):
    feList = []
    for file in os.listdir(path):
        if file.startswith("0x"):
            frontEnd = file.split('_')[0]
            if frontEnd in feList:
                pass
            else:
                feList.append(frontEnd)
    print(feList)
    return(feList)


def getNewFeNumbers():
    newFeList = []
    while True:
        inp = input(('Please enter your new Frontend numbers. Press "d" to use default numbers. \n'))
        if inp == "":
            print(f"Your entered FE values are: {newFeList}")
            inp = input("If this is ok, press enter. If you want to enter your number again, press any other key.")
            if inp == "":
                break
            else:
                getNewFeNumbers()
        newFeList.append(inp)
        if inp == 'd':
            newFeList = DEFAULT_FES
            break
    print(newFeList)
    return(newFeList)


def replaceOldNumbers(path, feList, newFeList):
    for file in os.listdir(path):
        for fe in feList:
            if fe in file:
                oldPath = os.path.join(path, file)
                i = feList.index(fe)
                try:
                    file = file.replace(fe, newFeList[i])
                    newPath = os.path.join(path, file)
                    os.rename(oldPath, newPath)
                    print(f"Renamed {oldPath} to {newPath}")
                except IndexError:
                    print(f"File {oldPath} is exceeding number of new Front Ends available. Please check your data or enter another frontend.")
                    inp = input("If you want to continue please press enter.")
                    if inp == "":
                        pass
                except FileNotFoundError:
                        print(f"Error: File {oldPath} not found.")
                except PermissionError:
                        print(f"Error: Permission denied to rename file {oldPath}.")
                except Exception as e:
                        print(f"An error occurred while renaming file {oldPath}: {e}")


if __name__ == "__main__": 
    path = getPath()
    feList = loop(path)
    replaceOldNumbers(path, feList, getNewFeNumbers())